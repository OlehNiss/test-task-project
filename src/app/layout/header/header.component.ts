import {ChangeDetectionStrategy, Component} from '@angular/core';
import { Router } from '@angular/router';
import { LoginService } from 'src/app/shared/services/login.service';
import { take } from "rxjs";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class HeaderComponent {

  constructor(
    private router: Router,
    private loginService: LoginService
  ) { }

  public logout (): void {
    this.loginService.logout();
  }
}
