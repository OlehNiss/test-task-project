import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {Customer} from "../../shared/models/Customer.interace";
import {Router} from "@angular/router";

@Component({
  selector: 'app-add-item',
  templateUrl: './add-item.component.html',
  styleUrls: ['./add-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class AddItemComponent implements OnInit {

  constructor(
    private router: Router
  ) { }

  ngOnInit(): void {}

  public passFormDataEvent (newItemData: Customer): void {
    const existingItemsArray = JSON.parse(localStorage.getItem('itemsData') || '[]');
    if (existingItemsArray.length) {
      newItemData.id = existingItemsArray[existingItemsArray.length - 1].id + 1;
    } else {
      newItemData.id = 0;
    }

    existingItemsArray.push(newItemData);

    this.saveData(existingItemsArray);
  }

  private saveData (existingItemsArray: Customer[]): void {
    localStorage.setItem('itemsData', JSON.stringify(existingItemsArray));
    this.router.navigate(['list']);
  }
}
