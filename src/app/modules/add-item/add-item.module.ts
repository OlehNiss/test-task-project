import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddItemRoutingModule } from './add-item-routing.module';
import {AddItemComponent} from "./add-item.component";
import {ItemFormModule} from "../../shared/modules/item-form/item-form.module";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";


@NgModule({
  declarations: [
    AddItemComponent
  ],
  imports: [
    CommonModule,
    AddItemRoutingModule,
    ItemFormModule,

    FormsModule,
    ReactiveFormsModule
  ]
})
export class AddItemModule { }
