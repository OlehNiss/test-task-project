import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import { AbstractControl, FormBuilder, FormGroup, Validators } from '@angular/forms';
import { LoginService } from 'src/app/shared/services/login.service';
import { take } from "rxjs";
import { Router } from "@angular/router";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class LoginComponent implements OnInit {
  public loginForm!: FormGroup;
  public isIncorrectMessage: boolean = false;
  public isPassShown: boolean = false;

  public get userNameControl (): AbstractControl {
    return this.loginForm.controls['userName'];
  }

  public get passwordControl (): AbstractControl {
    return this.loginForm.controls['password'];
  }

  constructor(
    private fb: FormBuilder,
    private loginService: LoginService,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.loginForm = this.fb.group({
      userName: [null, [Validators.required]],
      password: [null, [Validators.required]]
    })
  }

  public loginSubmit (): void {
    if (this.loginForm.valid) {
      const reqData = this.loginForm.getRawValue();

      this.loginService.login(reqData.userName, reqData.password)
        .pipe(take(1))
        .subscribe(
          (response: string) => {
            if (response && response === 'OK') {
              this.router.navigate(['list']);
            }
          },
          (error: any) => {
            this.isIncorrectMessage = true;
            console.log(error)
          }
        );
    }
  }

  public showHidePass (): void {
    this.isPassShown = !this.isPassShown;
  }
}
