import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import { Customer } from 'src/app/shared/models/Customer.interace';
import {ActivatedRoute, Router} from "@angular/router";

@Component({
  selector: 'app-edit-item',
  templateUrl: './edit-item.component.html',
  styleUrls: ['./edit-item.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class EditItemComponent implements OnInit {
  public itemId!: number;
  public editFormData!: Customer;

  constructor(
    private router: Router,
    private route: ActivatedRoute
  ) { }

  ngOnInit(): void {
    this.itemId = parseInt(this.route.snapshot.params['id']);

    this.editFormData = this.getSpecificItemData(this.itemId);
  }

  private getSpecificItemData (itemId: number): Customer {
    return JSON.parse(localStorage.getItem('itemsData')!).find((elem: Customer) => elem.id === itemId);
  }

  public passFormDataEvent (itemNewData: Customer): void {
    const editedArray = this.editItemDataInArray(itemNewData);

    this.saveEditedData(editedArray);
  }

  private editItemDataInArray (itemNewData: Customer): Customer[] {
    const existingItemsArray = JSON.parse(localStorage.getItem('itemsData') || '[]');

    const editedArray = existingItemsArray.map((item: Customer) => {
      if (item.id === itemNewData.id) {
        return itemNewData
      } else return item
    })

    return editedArray
  }

  private saveEditedData (editedArray: Customer[]): void {
    localStorage.setItem('itemsData', JSON.stringify(editedArray));
    this.router.navigate(['list']);
  }
}
