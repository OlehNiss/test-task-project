import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditItemRoutingModule } from './edit-item-routing.module';
import {EditItemComponent} from "./edit-item.component";
import {ItemFormModule} from "../../shared/modules/item-form/item-form.module";


@NgModule({
  declarations: [
    EditItemComponent
  ],
  imports: [
    CommonModule,
    EditItemRoutingModule,

    ItemFormModule
  ]
})
export class EditItemModule { }
