import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {EditItemComponent} from "./edit-item.component";

const routes: Routes = [
  {
    path: '',
    children: [
      {
        path: ':id',
        component: EditItemComponent
      }
    ]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EditItemRoutingModule { }
