import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ListItemsRoutingModule } from './list-items-routing.module';
import {ListItemsComponent} from "./list-items.component";
import {MatTableModule} from "@angular/material/table";
import {MatIconModule} from "@angular/material/icon";
import {MatTooltipModule} from "@angular/material/tooltip";
import {MatPaginatorModule} from "@angular/material/paginator";

@NgModule({
  declarations: [
    ListItemsComponent
  ],
  imports: [
    CommonModule,
    ListItemsRoutingModule,

    MatTableModule,
    MatIconModule,
    MatTooltipModule,
    MatPaginatorModule
  ]
})
export class ListItemsModule { }
