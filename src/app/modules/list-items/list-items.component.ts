import { AfterViewInit, ChangeDetectionStrategy, Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource } from "@angular/material/table";
import { Customer } from "../../shared/models/Customer.interace";
import { Router } from "@angular/router";
import { MatPaginator } from "@angular/material/paginator";
import { ListService } from 'src/app/shared/services/list.service';
import {take} from "rxjs";

@Component({
  selector: 'app-list-items',
  templateUrl: './list-items.component.html',
  styleUrls: ['./list-items.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ListItemsComponent implements OnInit, AfterViewInit {
  @ViewChild('paginator') paginator!: MatPaginator;

  public dataSource: MatTableDataSource<Customer> = new MatTableDataSource<Customer>([]);

  public readonly columnsNames = ['id', 'fullName', 'email', 'phoneNumber', 'age', 'editCol', 'deleteCol'];

  constructor(
    private router: Router,
    private listService: ListService
  ) { }

  ngOnInit(): void {
    this.getItemsList();
  }

  ngAfterViewInit (): void {
    this.dataSource.paginator = this.paginator;
  }

  private getItemsList (): void {
    const itemsArray = JSON.parse(localStorage.getItem('itemsData') || '[]');

    this.dataSource = new MatTableDataSource<Customer>(itemsArray);
  }

  public addTestItems (): void {
    if (this.dataSource.data.length) return;

    this.listService.getMockedItems()
      .pipe(take(1))
      .subscribe((mockedItems: Customer[]) => {
        localStorage.setItem('itemsData', JSON.stringify(mockedItems));
        this.dataSource = new MatTableDataSource<Customer>(mockedItems);
        this.dataSource.paginator = this.paginator;
      });
  }

  public editItem (id: number): void {
    this.router.navigate(['edit', id]);
  }

  public deleteItem (id: number): void {
    const newArray = this.deleteItemFromArray(id);

    this.saveDataAfterDelete(newArray);
  }

  private deleteItemFromArray (id: number): Customer[] {
    const existingItemsArray = JSON.parse(localStorage.getItem('itemsData') || '[]');

    const newArray: Customer[] = existingItemsArray.filter((item: Customer) => {
      return item.id !== id;
    });

    return newArray
  }

  private saveDataAfterDelete (newArray: Customer[]): void {
    localStorage.setItem('itemsData', JSON.stringify(newArray));
    this.dataSource = new MatTableDataSource<Customer>(newArray);
    this.dataSource.paginator = this.paginator;
  }
}
