import { Injectable } from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CustomValidators} from "../helpers/customValidators";

@Injectable({
  providedIn: 'root'
})
export class ItemFormsService {

  constructor(
    private fb: FormBuilder
  ) { }

  public getInitialForm (): FormGroup {
    return this.fb.group({
      id: [null],
      fullName: [null,
        [
          Validators.required,
          Validators.minLength(3)
        ]],
      email: [null,
        [
          Validators.required,
          Validators.pattern(CustomValidators.EMAIL)
        ]],
      phoneNumber: [null,
        [
          Validators.required,
          Validators.minLength(12),
          Validators.pattern(CustomValidators.PHONE)
        ]],
      age: [null, [Validators.required]]
    })
  }
}
