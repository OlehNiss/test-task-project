import { Injectable } from '@angular/core';
import {BehaviorSubject, Observable, of, throwError} from "rxjs";
import {mockedUser} from "../models/mockedUser";
import {Router} from "@angular/router";

@Injectable({
  providedIn: 'root'
})
export class LoginService {
  private readonly adminUser: any = mockedUser;
  public readonly jwtToken = new BehaviorSubject<string | null>(null);

  constructor(
    private router: Router
  ) { }

  // TODO: a few users with some roles with different permissions
  public login (userName: string, password: string): Observable<string> {
    if (userName === this.adminUser.userName && password === this.adminUser.password) {
      const fakeJwtToken = 'someFakeJwtToken'
      localStorage.setItem('fakeJwtToken', fakeJwtToken);
      this.jwtToken.next(fakeJwtToken);

      return of('OK')
    } else {
      return throwError('Username or password are incorrect');
    }
  }

  public logout (): void {
    localStorage.removeItem('fakeJwtToken');
    this.jwtToken.next(null);

    this.router.navigate(['login']);
  }

  public autoLogin (): void {
    const jwtToken = localStorage.getItem('fakeJwtToken');

    if (!jwtToken) return;

    this.jwtToken.next(jwtToken);
  }
}
