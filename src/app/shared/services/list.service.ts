import { Injectable } from '@angular/core';
import {MockedCustomers} from "../models/mockedListItems";
import {Observable, of} from "rxjs";
import { Customer } from '../models/Customer.interace';

@Injectable({
  providedIn: 'root'
})
export class ListService {
  private mockedData = MockedCustomers

  constructor() { }

  public getMockedItems (): Observable<Customer[]> {
    return of(this.mockedData);
  }
}
