import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ItemFormComponent } from './item-form.component';
import {FormsModule, ReactiveFormsModule} from "@angular/forms";



@NgModule({
  declarations: [
    ItemFormComponent
  ],
  imports: [
    CommonModule,

    FormsModule,
    ReactiveFormsModule
  ],
  exports: [
    ItemFormComponent
  ]
})
export class ItemFormModule { }
