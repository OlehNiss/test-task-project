import {ChangeDetectionStrategy, Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {AbstractControl, FormGroup} from '@angular/forms';
import {ItemFormsService} from "../../services/item-forms.service";
import {Customer} from "../../models/Customer.interace";
import {Router} from "@angular/router";

@Component({
  selector: 'app-item-form',
  templateUrl: './item-form.component.html',
  styleUrls: ['./item-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class ItemFormComponent implements OnInit {
  @Input() editFormData!: Customer;
  @Input() itemId!: number;

  @Output() passFormDataEvent  = new EventEmitter<Customer>();

  public itemForm: FormGroup = this.itemFormsService.getInitialForm();

  public get fullNameControl (): AbstractControl {
    return this.itemForm.controls['fullName'];
  }

  public get emailControl (): AbstractControl {
    return this.itemForm.controls['email'];
  }

  public get ageControl (): AbstractControl {
    return this.itemForm.controls['age'];
  }

  public get phoneNumberControl (): AbstractControl {
    return this.itemForm.controls['phoneNumber'];
  }

  constructor(
    private itemFormsService: ItemFormsService,
    private router: Router
  ) { }

  ngOnInit(): void {
    if (this.editFormData) {
      this.itemForm.setValue(this.editFormData);
    }
  }

  public submitForm (): void {
    if (this.itemForm.valid) {
      this.passFormDataEvent.emit(this.itemForm.getRawValue());
    }
  }

  public cancelAdd (): void {
    this.router.navigate(['list']);
  }
}
