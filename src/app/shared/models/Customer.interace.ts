export interface Customer {
  id: number,
  fullName: string,
  email: string,
  phoneNumber: string,
  age: number
}
