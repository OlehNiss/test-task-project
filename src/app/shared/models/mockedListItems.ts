import { Customer } from "./Customer.interace";

export const MockedCustomers: Customer[] = [
  {
    id: 0,
    fullName: "Oleh Niss",
    email: "oleh123@gmail.com",
    phoneNumber: "+380668445945",
    age: 21
  },
  {
    id: 1,
    fullName: "Oleg Ivanov",
    email: "oleg123@gmail.com",
    phoneNumber: "+380895658656",
    age: 23
  },
  {
    id: 2,
    fullName: "Maks Petrenko",
    email: "makspetrenko@gmail.com",
    phoneNumber: "+386562226626",
    age: 31
  },
  {
    id: 3,
    fullName: "Paul West",
    email: "paulwest@gmail.com",
    phoneNumber: "+389555521452",
    age: 28
  },
  {
    id: 4,
    fullName: "Ruslan Teteriv",
    email: "ruslanteterivf@gmail.com",
    phoneNumber: "+596559959959",
    age: 15
  },
  {
    id: 5,
    fullName: "Ivan Ivanenko",
    email: "ivanivanenko@gmail.com",
    phoneNumber: "+745559959959",
    age: 29
  },
  {
    id: 6,
    fullName: "Andrii Tkach",
    email: "andriitkach@gmail.com",
    phoneNumber: "+859559959959",
    age: 31
  },
  {
    id: 7,
    fullName: "John Smith",
    email: "johnsmith@gmail.com",
    phoneNumber: "+384859959959",
    age: 23
  },
  {
    id: 8,
    fullName: "Sam White",
    email: "samwhite@gmail.com",
    phoneNumber: "+485559959959",
    age: 42
  }
]
