import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './shared/guards/auth.guard';
import { ListGuard } from './shared/guards/list.guard';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./modules/login/login.module').then(m => m.LoginModule),
    canActivate: [ListGuard]
  },
  {
    path: 'list',
    loadChildren: () =>  import ('./modules/list-items/list-items.module').then(m => m.ListItemsModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'add',
    loadChildren: () => import ('./modules/add-item/add-item.module').then(m => m.AddItemModule),
    canActivate: [AuthGuard]
  },
  {
    path: 'edit',
    loadChildren: () => import ('./modules/edit-item/edit-item.module').then(m => m.EditItemModule),
    canActivate: [AuthGuard]
  },
  {
    path: '**',
    redirectTo: '/list',
    pathMatch: 'full'
  },
  {
    path: '',
    redirectTo: '/list',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
