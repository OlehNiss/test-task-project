# TestProject1

There is Login page at the start of the application.

To log in and check the app, you have to input this credentials:

userName: `admin123`,

password: `qwerty1$`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.
